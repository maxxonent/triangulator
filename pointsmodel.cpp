#include "pointsmodel.h"

extern TriangulatorCore *core;
extern QQmlApplicationEngine *view;


PointsModel::PointsModel(QObject *parent) : QAbstractListModel(parent)
{
    connect(core, &TriangulatorCore::pointsAdded, this, &PointsModel::update);
}


void PointsModel::update()
{
    addPoint();
//    qDebug() << "model updated! points";
}

void PointsModel::buttonAdd(const int x, const int y)
{
    qDebug() << "button add "<< x << " " << y;
    core->addPoint(x, y);
}


void PointsModel::addPoint()
{
    points = core->getPointsVector();
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        m_data_points.append(points.last());
        m_data_description.append(
                    "Point number " + QString::number(m_data_points.count()) + " has coordinates:\n" +
                    "\tx = " + QString::number(points.last().x() ) +
                    ", y = " + QString::number(points.last().y() )
                    );
        qDebug() << "last point is " << m_data_points.last() << " And there is count of points in model: " << rowCount();
        qDebug() << "And really, m_data_points elemens count is " << m_data_points.count();
        endInsertRows();
}

void PointsModel::resetModel()
{
    beginResetModel();
    m_data_points.clear();
    m_data_description.clear();
    endResetModel();
}

int PointsModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    int size = m_data_points.count();
    return size;
}

QHash<int, QByteArray> PointsModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles[DescriptionRole] = "descriptionRole";
    roles[PointRoleX] = "pointRoleX";
    roles[PointRoleY] = "pointRoleY";
    return roles;
}


QVariant PointsModel::data(const QModelIndex &index, int role) const
{
    qDebug() << "Checked row: " << index.row();
    if (!index.isValid()) {
        return QVariant();
    }
    switch (role) {
    case DescriptionRole:
        return m_data_description.at(index.row());
    case PointRoleX:
        return m_data_points.at(index.row()).x();
    case PointRoleY:
        return m_data_points.at(index.row()).y();

    default:
        return QVariant();
    }

}

