#include "core.h"
#include "ui_mainwindow.h"




TriangulatorCore::TriangulatorCore(QObject *parent)
{
    Q_UNUSED(parent);
}


QVector<Triangle> TriangulatorCore::getTrianglesVector()
{
    return triangles;
}

QVector<QPoint> TriangulatorCore::getPointsVector()
{
    return points;
}

void TriangulatorCore::updatePointsList()
{
}

void TriangulatorCore::addPoint(const int &x, const int &y)
{
    QPoint point( x, y );
    points.append(point);
//    qDebug() << "addPoint func! pointsAdded emitted next!" << point;

    emit pointsAdded();

}

void TriangulatorCore::triangulate()
{
    int direction;
    int triangleCounter;
    triangleCounter = 0;
    direction = getAngleDirection(getUpperOfRightPointsIndex());

    while( points.size() >= 3 )
    {
        int i = 0;
        while ( i < points.size())
        {
//            qDebug() << "Working on point number " << i;
            QPoint a = points.at(i);
            QPoint b = getNextPoint(i);
            QPoint c = getPrevPoint(i);
            if( isAngleDirectionCorrect(i, direction) )
            {
                if(isPointsInside(a,b,c) != true)
                {
                    if(isTriangleFlat(a,b,c) != true)
                    {
                            points.remove(i);
                            triangleCounter++;

                            QString currentTriangleMessage;
                            currentTriangleMessage = "Triangle number " + QString::number(triangleCounter) + "\n"
                                    + "\tFirst point x = " + QString::number(a.x())
                                    + ", y = " + QString::number(a.y()) +  "\n"
                                    + "\tSecond point x = " + QString::number(b.x())
                                    + ", y = " + QString::number(b.y()) +  "\n"
                                    + "\tThird point x = " + QString::number(c.x())
                                    + ", y = " + QString::number(c.y()) +  "\n";
                            Triangle triangle(a,b,c);
                            triangles.append(triangle);
//                            qDebug() << currentTriangleMessage;


                    }
                    else
                    {
//                        qDebug() << "triangle is flat!" <<a<<b<<c;
                    }

                }
                else
                {
//                    qDebug() << "some points inside!" << a << b << c ;
                }

            }
            else
            {
//                qDebug() << "angle directiom incorrect!" << a << b << c ;
            }

            i++;
        }
    }
    emit trianglesAdded();

}


QPoint TriangulatorCore::getNextPoint(const int i)
{
    if ( i == (points.size()-1))
    {
        return points.at(0);
    }
    return points.at(i+1);
}

QPoint TriangulatorCore::getPrevPoint(const int i)
{
    if ( i == 0 )
    {
        return points.at(points.size() - 1);
    }
    return points.at(i-1);
}


bool TriangulatorCore::isPointsInside( const QPoint &a, const QPoint &b, const QPoint &c )
{

    QPoint ab = getRadiusVector(a,b);
    QPoint bc = getRadiusVector(b,c);
    QPoint ca = getRadiusVector(c,a);


    for (QPoint point: points)
    {
        if( point != a && point != b && point != c )
        {
            if ((vecMultiply(ab, getRadiusVector(a, point)) < 0
                 &&
                 vecMultiply(bc, getRadiusVector(b, point)) < 0
                 &&
                 vecMultiply(ca, getRadiusVector(c, point)) < 0)
                    ||
                    (vecMultiply(ab, getRadiusVector(a, point)) > 0
                     &&
                     vecMultiply(bc, getRadiusVector(b, point)) > 0
                     &&
                     vecMultiply(ca, getRadiusVector(c, point)) > 0)
                    )
            {
                return true;
            }


            if(isPointsOnOneLine(a, b, point))
            {
                if(
                        (point.x() <= a.x() && point.x() >= b.x())
                        ||
                        (point.x() >= a.x() && point.x() <= b.x())
                        ||
                        (point.y() <= a.y() && point.y() >= b.y())
                        ||
                        (point.y() >= a.y() && point.y() <= b.y())
                        )
                {
                    return true;
                }
            }
            if(isPointsOnOneLine(b, c, point))
            {
                if(
                        (point.x() <= b.x() && point.x() >= c.x())
                        ||
                        (point.x() >= b.x() && point.x() <= c.x())
                        ||
                        (point.y() <= b.y() && point.y() >= c.y())
                        ||
                        (point.y() >= b.y() && point.y() <= c.y())
                        )
                {
                    return true;
                }
            }
            if(isPointsOnOneLine(c, a, point))
            {
                if(
                        (point.x() <= c.x() && point.x() >= a.x())
                        ||
                        (point.x() >= c.x() && point.x() <= a.x())
                        ||
                        (point.y() <= c.y() && point.y() >= a.y())
                        ||
                        (point.y() >= c.y() && point.y() <= a.y())
                        )
                {
                    return true;
                }
            }
        }
    }
    return false;
}

QPoint TriangulatorCore::getRadiusVector(const QPoint &a, const QPoint &b)
{
    QPoint pointToReturn;
    pointToReturn.setX(b.x()-a.x());
    pointToReturn.setY(b.y()-a.y());
    return pointToReturn;
}

int TriangulatorCore::vecMultiply(QPoint vecA, QPoint vecB)
{
    int result = vecA.x()*vecB.y()-vecA.y()*vecB.x();
    return result;
}

bool TriangulatorCore::isTriangleFlat(const QPoint &a, const QPoint &b, const QPoint &c)
{
    if(isPointsOnOneLine(a,b,c) == true)
    {
        qDebug() << "Triangle is flat! " << a << b << c;
        return true;
    }
    return false;
}

bool TriangulatorCore::isPointsOnOneLine(const QPoint &a, const QPoint &b, const QPoint &c)
{
    QPoint ab = getRadiusVector(a,b);
    QPoint ac = getRadiusVector(a,c);
    int vec = vecMultiply(ab,ac);
    if(vec == 0)
    {
        return true;
    }
    return false;
}

void TriangulatorCore::openFileDialog()
{
    QFileDialog openFile;
    QString path;
    path = openFile.getOpenFileName();
    QFile openedFile;
    openedFile.setFileName(path);
    openedFile.open(QIODevice::ReadOnly);
    QTextStream stream(&openedFile);
    fillPointsVectorFromFile(stream);
    updatePointsList();
}


void TriangulatorCore::fillPointsVectorFromFile(QTextStream &stream)
{
    points.clear();
    while(!stream.atEnd())
    {
        QString line = stream.readLine();
        QPoint point = parseLine(line);
        points.append(point);
//        qDebug() << "point appended" << point;

    }

}

QPoint TriangulatorCore::parseLine(const QString &string)
{
    QPoint point;
    QStringList list;
    list = string.split(",");
    point.setX(list.at(0).toInt());
    point.setY(list.at(1).toInt());
//    qDebug() << "point added" << point;
    return point;
}

int TriangulatorCore::getUpperOfRightPointsIndex()
{
    QPoint UpperOfRightPoints(0,0);
    QVector<int> rightpointsIds;
    int maxX = 0;
    int pointId = 0;
    for(QPoint point: points)     // search for max x
    {
        if(point.x() > maxX)
        {
            maxX = point.x();
        }
    }
    for (int i = 0; i < points.size(); i++) // search for ids of right points
    {
        if (points.at(i).x() == maxX)
        {
            rightpointsIds.append(i);
        }
    }
    for(int id: rightpointsIds)
    {
        if(points.at(id).y() > points.at(pointId).y())
        {
            pointId = id;
        }
    }
    return pointId;
}

int TriangulatorCore::getAngleDirection(const int &i)
{
    QPoint a = points.at(i);
    QPoint b = getNextPoint(i);
    QPoint c = getPrevPoint(i);
    QPoint ab = getRadiusVector(a,b);
    QPoint ac = getRadiusVector(a,c);
    int dir = vecMultiply(ab, ac);
    return dir;
}

bool TriangulatorCore::isAngleDirectionCorrect(const int &i, const int &direction)
{
    if( ((getAngleDirection(i) > 0) && (direction > 0)) ||
       ((getAngleDirection(i) < 0) && (direction < 0)) )
    {
        return true;
    }
    return false;
}
