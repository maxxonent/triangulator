#ifndef TRIANGLE_H
#define TRIANGLE_H
#include <QPoint>
#include <QObject>


class Triangle : public QObject
{
    Q_OBJECT

public:
    explicit Triangle(const Triangle &triangle);
    explicit Triangle(const QPoint a, const QPoint b, const QPoint c);
    explicit Triangle();

    Q_INVOKABLE QPoint getA() const;
    Q_INVOKABLE QPoint getB() const;
    Q_INVOKABLE QPoint getC() const;


    void setTriangleFromPoints(const QPoint &a, const QPoint &b, const QPoint &c);

    Triangle& operator=(const Triangle &triangle);

private:
    QPoint m_a;
    QPoint m_b;
    QPoint m_c;
};

Q_DECLARE_METATYPE(Triangle)

#endif // TRIANGLE_H
