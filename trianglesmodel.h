#ifndef TRIANGULATORMODEL_H
#define TRIANGULATORMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QtQuick>
#include <core.h>


class TrianglesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        DescriptionRole = Qt::UserRole + 1,
        TriangleRoleA,
        TriangleRoleB,
        TriangleRoleC
    };

    TrianglesModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;


    QHash<int, QByteArray> roleNames() const;
public slots:
    Q_INVOKABLE void update();
    Q_INVOKABLE void openFile();
    Q_INVOKABLE void buttonTriangulate();

private:
    QStringList m_data_description;
    QVector<Triangle> m_data_triangles;
    void setData();
    void fillTriangleslist();
    void resetModel();
    QObject* myParent;

};

#endif // TRIANGULATORMODEL_H
