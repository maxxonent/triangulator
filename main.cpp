#include "core.h"
#include <QApplication>
#include <trianglesmodel.h>
#include <pointsmodel.h>
#include <QtQml>

TriangulatorCore *core;
QQmlApplicationEngine *view;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qmlRegisterType<TrianglesModel>("Models", 1, 0, "TrianglesModel");
    qmlRegisterType<PointsModel>("Models", 1, 0, "PointsModel");
    qmlRegisterType<Triangle>("TriangleData", 1, 0, "TriangleData");
    qRegisterMetaType<Triangle>();

    core = new TriangulatorCore();

    view = new QQmlApplicationEngine(core);
    view->load(QUrl(QStringLiteral("qrc:///main.qml")));

    return app.exec();
}
