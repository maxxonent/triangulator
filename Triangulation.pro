#-------------------------------------------------
#
# Project created by QtCreator 2016-08-29T10:19:50
#
#-------------------------------------------------

QT       += core qml quick

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = Triangulation
TEMPLATE = app


SOURCES += main.cpp \
    triangle.cpp \
    trianglesmodel.cpp \
    pointsmodel.cpp \
    core.cpp

HEADERS  += \
    triangle.h \
    pointsmodel.h \
    trianglesmodel.h \
    core.h

FORMS    +=

DISTFILES +=

RESOURCES += \
    view.qrc
