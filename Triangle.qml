import QtQuick 2.0
import TriangleData 1.0

Canvas {
    anchors.fill: parent
    id: itemTriangle
    property color lineColor: "red"
    property color fillColor: "yellow"

//    property TriangleData triangleData
    property point trianglepointA: Qt.point(0,0)
    property point trianglepointB: Qt.point(0,0)
    property point trianglepointC: Qt.point(0,0)

    onPaint: {
        var ctx = getContext("2d")
        var a = trianglepointA
        var b = trianglepointB
        var c = trianglepointC
        var scaleFactor = 15

        //ctx.clearRect(0, 0, item.width, item.height);
        ctx.lineWidth = 1
        ctx.strokeStyle = itemTriangle.lineColor
        ctx.fillStyle = itemTriangle.fillColor
        ctx.save()
        ctx.beginPath()
        ctx.moveTo(a.x*scaleFactor, a.y*scaleFactor)
        ctx.lineTo(b.x*scaleFactor, b.y*scaleFactor)
        ctx.lineTo(c.x*scaleFactor, c.y*scaleFactor)
        ctx.lineTo(a.x*scaleFactor, a.y*scaleFactor)
        ctx.fill()
        ctx.stroke()
    }

}

