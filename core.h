#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPoint>
#include <QVector>
#include <QString>
#include <QDebug>
#include <QFile>
#include <QFile>
#include <QFileDialog>
#include <QStringListIterator>
#include <QMessageBox>
#include <triangle.h>
#include <QObject>


class TriangulatorCore: public QObject
{
    Q_OBJECT

public:
    TriangulatorCore(QObject *parent = 0);
    QVector<Triangle> getTrianglesVector();

    TriangulatorCore *getPointerToWindow();
    QVector<QPoint> getPointsVector();
public slots:
    void triangulate();
    void openFileDialog();
    void addPoint(const int &x, const int &y);

signals:
    void trianglesAdded();
    void pointsAdded();

private:
    QVector<QPoint> points;
    QVector<Triangle> triangles;
    QString logString;
    QString triangleslist;
    TriangulatorCore* pointerToMe;

    int getIndexOfRightPoint();
    int findDirection();
    QPoint getNextPoint(const int i);
    QPoint getPrevPoint(const int i);
    bool isAngleConvex(const QPoint &a, const QPoint &b, const QPoint &c, const int z);
    void checkDirection(int direction, int id_right, int id_right_next);
    bool isPointsInside( const QPoint &a, const QPoint &b, const QPoint &c );
    QPoint getRadiusVector(const QPoint &a, const QPoint &b);
    int vecMultiply(QPoint vecA, QPoint vecB);
    bool isTriangleFlat(const QPoint &a, const QPoint &b, const QPoint &c);
    bool isNextTriangleFlat(const int i);
    void fillPointsVectorFromFile( QTextStream &stream);
    QPoint parseLine(const QString &string);
    void updatePointsList();
    int getUpperOfRightPointsIndex();
    int getAngleDirection(const int &i);
    bool isAngleDirectionCorrect(const int &i, const int &direction);
    bool isPointsOnOneLine(const QPoint &a, const QPoint &b, const QPoint &c);
};


#endif // MAINWINDOW_H
