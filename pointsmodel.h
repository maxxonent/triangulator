#ifndef POINTSMODEL_H
#define POINTSMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QtQuick>
#include <core.h>

class PointsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        DescriptionRole = Qt::UserRole + 1,
        PointRoleX,
        PointRoleY
    };

    PointsModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;


    QHash<int, QByteArray> roleNames() const;
public slots:
    Q_INVOKABLE void update();
    Q_INVOKABLE void buttonAdd(const int x, const int y);


private:
    QStringList m_data_description;
    QVector<QPoint> points;
    QVector<QPoint> m_data_points;
    void setData();
    void addPoint();
    QObject* myParent;
    void resetModel();


};

#endif // POINTSMODEL

