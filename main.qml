import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Layouts 1.2
import Models 1.0

ApplicationWindow {
    width: 700
    height: 500
    title: qsTr("Triangulation App")
    visible: true
    id: mainWindow
    property string pointsList
    property string trianglesList

    signal buttonClicked()

    menuBar: MenuBar {
        Menu {
            title: "Файл"
            visible: true
            MenuItem {
                text: "Открыть"
            onTriggered: trianglesModel.openFile()
            }
            MenuItem {
                text: "Завершить приложение"
            onTriggered: Qt.quit()}
        }
    }

    TrianglesModel {
        id: trianglesModel
    }

    PointsModel {
        id: pointsModel
    }

    SpinBox {
        id: spinBoxX
        x: 11
        y: 57
    }

    SpinBox {
        id: spinBoxY
        x: 60
        y: 57
    }

    Button {
        id: buttonAdd
        x: 109
        y: 57
        text: qsTr("Добавить точку")
        onClicked: pointsModel.buttonAdd(spinBoxX.value, spinBoxY.value)
    }

    ScrollView {
        id: scrollViewOfPoints
        x: 11
        y: 88
        width: 226
        height: 193
        frameVisible: true

        ListView {
            id: listViewOfPoints
            anchors.fill: parent
            model: pointsModel
            delegate: Text{
                text: model.descriptionRole
            }
        }
    }



    Button {
        id: buttonTriangulate
        x: 11
        y: 287
        width: 226
        height: 27
        text: qsTr("Triangulate")
        onClicked: trianglesModel.buttonTriangulate()
    }

    ScrollView {
        x: 5
        y: 320
        width: 240
        height: 163
        frameVisible: true

        ListView {
            id: listViewOfTriangles
            anchors.fill: parent
            model: trianglesModel
            delegate: Text {
                    text: model.descriptionRole
                    height: contentHeight
                }
            }
    }

    Text {
        id: text1
        x: 47
        y: 34
        text: qsTr("X")
        font.pixelSize: 12
    }

    Text {
        id: text2
        x: 93
        y: 34
        width: 10
        height: 17
        text: qsTr("Y")
        font.pixelSize: 12
    }

    Item {
        id: fieldForTriangles
        x: 249
        y: 57
        width: 400
        height: 426
        Repeater {
            model: trianglesModel
            delegate: Triangle {
                parent: fieldForTriangles
                trianglepointA: model.triangleRoleA
                trianglepointB: model.triangleRoleB
                trianglepointC: model.triangleRoleC
            }
            //            transform: Scale {xScale: 50; yScale: 50}

        }
        Repeater {
            model: pointsModel
            delegate: Item {
                id: rootPoint
                x: model.pointRoleX*15
                y: model.pointRoleY*15
                Rectangle {
                    height: 3
                    width: 3
                    color: "blue"
                    anchors.centerIn: rootPoint
                    Component.onCompleted: console.log("some point is added!" + model.pointRoleX + " " + model.pointRoleY)
                }

            }
        }
    }


}
