#include "trianglesmodel.h"

extern TriangulatorCore *core;
extern QQmlApplicationEngine *view;


TrianglesModel::TrianglesModel(QObject *parent) : QAbstractListModel(parent)
{
    connect(core, &TriangulatorCore::trianglesAdded, this, &TrianglesModel::update);
}


void TrianglesModel::update()
{
    fillTriangleslist();
    qDebug() << "model updated! Triangles";
}

void TrianglesModel::fillTriangleslist()
{
    resetModel();
    m_data_triangles = core->getTrianglesVector();

    for(int i = 0; i < m_data_triangles.count(); i++)
    {
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        Triangle triangle(m_data_triangles.at(i));
        m_data_description.append("Triangle number " + QString::number(i+1) + " has coordinates:\n" +
                      "\tPoint A: x = " + QString::number(triangle.getA().x() ) +
                      ", y = " + QString::number(triangle.getA().y() ) + "\n" +
                      "\tPoint B: x = " + QString::number(triangle.getB().x() ) +
                      ", y = " + QString::number(triangle.getB().y() ) + "\n" +
                      "\tPoint C: x = " + QString::number(triangle.getC().x() ) +
                      ", y = " + QString::number(triangle.getC().y() ) + "\n" );
        endInsertRows();
    }
//    qDebug() << m_data_description;

}

void TrianglesModel::resetModel()
{
    beginResetModel();
    m_data_triangles.clear();
    m_data_description.clear();
    endResetModel();
}


int TrianglesModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    int size = m_data_description.size();
    return size;
}

QHash<int, QByteArray> TrianglesModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles[DescriptionRole] = "descriptionRole";
    roles[TriangleRoleA] = "triangleRoleA";
    roles[TriangleRoleB] = "triangleRoleB";
    roles[TriangleRoleC] = "triangleRoleC";

    return roles;
}
void TrianglesModel::buttonTriangulate()
{
    core->triangulate();
}

QVariant TrianglesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    switch (role) {
    case DescriptionRole:
        return m_data_description.at(index.row());
    case TriangleRoleA:
        return m_data_triangles.at(index.row()).getA();
    case TriangleRoleB:
        return m_data_triangles.at(index.row()).getB();
    case TriangleRoleC:
        return m_data_triangles.at(index.row()).getC();

    default:
        return QVariant();
    }

}

void TrianglesModel::openFile()
{
    core->openFileDialog();
}

