#include "triangle.h"


Triangle::Triangle(const Triangle &triangle) : QObject()
{
    setTriangleFromPoints(triangle.getA(), triangle.getB(), triangle.getC());
}

Triangle::Triangle(const QPoint a, const QPoint b, const QPoint c) : QObject()
{
    setTriangleFromPoints(a,b,c);
}

Triangle::Triangle() : QObject()
{

}

QPoint Triangle::getA() const
{
   return m_a;
}

QPoint Triangle::getB() const
{
   return m_b;
}

QPoint Triangle::getC() const
{
   return m_c;
}
void Triangle::setTriangleFromPoints(const QPoint &a, const QPoint &b, const QPoint &c)
{
    m_a = a;
    m_b = b;
    m_c = c;
}

Triangle& Triangle::operator=(const Triangle &triangle)
{
    m_a = triangle.getA();
    m_b = triangle.getB();
    m_c = triangle.getC();
    return *this;
}


